﻿using System;
using System.IO;

namespace MoveZeros
{
    class Program
    {
        public static void MoveZerosToEnd(int[] arr, int n)
        {
            //non-zero elements
            int count = 0;

            for (int i = 0; i < n; i++)
                if (arr[i] != 0)
                    arr[count++] = arr[i];

            while (count < n)
                arr[count++] = 0;
        }
        static void Main(string[] args)
        {
            int[] arr = { 1, 2, 0, 4, 3, 0, 5, 0 };
            int n = arr.Length;

            Console.WriteLine("Initial array: ");
            for (int i = 0; i < n; i++)
                Console.Write(arr[i] + " ");

            MoveZerosToEnd(arr, n);

            Console.WriteLine("\nArray after moving zeros: ");
            for (int i = 0; i < n; i++)
                Console.Write(arr[i] + " ");
        }
    }
}
