# CSharpCourse

## Course 1 
- Basics 

## Course 2 
- Implementati o aplicatie Calculator de tip consola care efectueaza urmatoarele operatii: adunare, scadere, inmulture, impartire, afisare rezultat. Operatiile se dau de la tastatura (definiti un switch, in care operatia se executa in functie de o tasta apasata).

- Definiti o clasa Animal, care contine informatii precum (Id, Name, Description) si o clasa Tiger, derivate din Animal. Implementati o clasa AnimalReposity care defineste operatii legate de adaugarea, editarea, listarea si stergerea unui animal. Stocati informatiile intr-o lista (ca la curs). Definiti o clasa AnimalController care utilizeaza functionalitatile din AnimalRepository. Abstractizati codul prin interfete si definiti unit teste pentru metodele din AnimalController, “mocuind” AnimalRepository. Utilizati sablonul exemplificat la curs.

- Sa se construiasca urmatoarea ierarhie de clase intr-un proiect de tip console app: 
	- clasa Persoana: nume (string), prenume (string), adresa (string) 
	- clasa Student extends Persoana: nrMatricol (string), medie (double) 
	- clasa Profesor extends Persoana:  gradProf(GradProfesor) 
	- enum GradProfesor: definitivat, grad_2, grad_1, doctorat 
	- In clasa cu metoda Main, in structura List<Persoana> personList = new List<Persoana>()se vor introduce obiecte de tip Student si Persoana. Afisati pe ecran lista sortata dupa nume, apoi dupa prenume. 

- Definiti o clasa care gestioneaza operatii cu arbori binari de cautare: adaugare, stergere, minim, maxim, parcurgeri – preordine, inordine, postordine. Operatiile se dau de la tastatura (definiti un switch, in care operatia se executa in functie de o tasta apasata).

## Course 3

- Write a program in C# Sharp to convert a string array read from a text file to a string, using LINQ Test Data : 
	- Input number of strings to store in the array :3
	- Input 3 strings for the array : 
	- Element[0] : cat 
	- Element[1] : dog 
	- Element[2] : rat 
	- Expected Output: cat, dog, rat
	- Hint: consider the method Join() of strings
	
- Write a program in C# Sharp to find the string which starts and ends with a specific character, using LINQ. The list of strings can be hardcoded. 
	- Test Data : 
	- The cities are : 'ROME','LONDON','NAIROBI','CALIFORNIA','ZURICH','NEW DELHI','AMSTERDAM','ABU DHABI','PARIS' 
	- Input starting character for the string : A 
	- Input ending character for the string : M
	- Expected Output : 
	- The city starting with A and ending with M is : AMSTERDAM

## Course 4

- Creati o aplicatie ASP.NET Core, pornind de la un model Empty, care afiseaza pe ecran informatii privind ziua curenta (data, ziua si ora). Ca model, definiti clasa "CustomDateTime", care va contine aceste informatii. Creati controller-ul si actiunea corespunzatoare si configurati setarile din Startup.cs (middleware, route etc). In controller, repository-ul e pasat prin intermediul constructorului.

- Creati o aplicatie ASP.NET Core, pornind de la un model Empty, care afiseaza pe ecran informatii privind o lista de produse in-memory. Ca model, definiti clasa "Product", care va contine aceste informatii (Id, Name, Description). Creati controller-ul si actiunea corespunzatoare si configurati setarile din Startup.cs (middleware, route etc). In controller, repository-ul e pasat prin intermediul constructorului. Definiti 2 actiuni si view-urile corespunzatoare: GetAll() si GetById(int id)

## Course 5

- Adaugati noi functionalitati la tema anterioara cu produse pentru a returna valorile dintr-o baza de date de tip SQL Server, folosind Entity Framework Core. Datele din baza de date se pot adauga manual. Pentru a crea migratiile, respective a actualiza baza de date se folosesc in Package Manager Console comenziile: add-migration [name] si update-database -v
- Se considera 2 vectori sortati crescator (cititi dintr-un fisier text). Scrieti un program C# care creeaza un al treilea vector ce contine elementele sortate ale celor 2 vectori de intrare. Vectorul rezultat se va afisa pe ecran.

	- Exemple:

	Input: v1[] = { 1, 3, 4, 5}, v2[] = {2, 4, 6, 8}
	Output: res[] = {1, 2, 3, 4, 4, 5, 6, 8}

	Input: v1[] = { 5, 8, 9}, v2[] = {4, 7, 8}
	Output: res[] = {4, 5, 7, 8, 8, 9}

- Se citeste dintr-un fisier text un vector. Scrieti un program C# care muta toate cifrele de zero la sfarsitul vectorului, fara a utiliza un alt vector auxiliar, si fara a schimba ordinea de aparitie a elementelor in vectorul curent. Scrieti rezultatul intr-un alt fisier text.

	- Example:

	Input: arr[] = {1, 2, 0, 4, 3, 0, 5, 0}
	Output: 1 2 4 3 5 0 0 0

	Input: arr[] = {1, 2, 0, 0, 0, 3, 6};
	Output: 1 2 3 6 0 0 0

- Se considerea urmatoarea metoda:

	public static int NumberOfOccurences(string inputString, Stream stream)
	{
		if (string.IsNullOrEmpty(inputString))
		{
			throw new ArgumentNullException(nameof(inputString));
		}

		if (stream == null)
		{
			throw new ArgumentNullException(nameof(stream));
		}

		if (!stream.CanRead)
		{
			throw new ArgumentException("Stream cannot be read");
		}

		// add your implementation here.
		throw new NotImplementedException();
	}

Completati metoda, astfel incat sa se returneze numarul de aparitii ale stringului de intrare (inputString) pe fiecare linie a continutului stream-ului dat de parametru.

De exemplu:

var inputTest = "abc";
// can use any kind of stream: MemoryStream, StreamReader etc.
var stream = new MemoryStream(Encoding.ASCII.GetBytes("abcTeST\n this is abc a test\n yes"));
Console.WriteLine(NumberOccurences(inputTest, stream)); // se va afisa 2

Implementarea trebuie sa fie una generica, valabila pentru orice tip de Stream (Stream e clasa de baza in C# pentru mai multe tipuri, ex: MemoryStream, StreamReader etc).
