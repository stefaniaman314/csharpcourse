﻿using Microsoft.AspNetCore.Mvc;
using Product.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Product.Controllers
{
    public class ProductController : Controller
    {
        readonly ProductRepository _productRepository;

        public ProductController(ProductRepository repo)
        {
            this._productRepository = repo;
        }

        public IActionResult Index()
        {
            var products = _productRepository.Products;
            return View(products);
        }
    }
}
