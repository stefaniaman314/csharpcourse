﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Product.Models
{
    public class ProductRepository
    {
        private List<Product> _products = new List<Product>
        {
            new Product
            {
                Id=1,
                Name="Milk",
                Description="Low Fat"
            },

            new Product
            {
                Id=2,
                Name="Coca-cola",
                Description="Zero Sugar"
            }
        };

        public List<Product> Products => _products;
    }
}
