﻿using DateTimeCustom.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DateTimeCustom.Controllers
{
    public class CustomDateTimeController : Controller
    {
        readonly CustomDateTimeRepository _repo;

        public CustomDateTimeController(CustomDateTimeRepository repo)
        {
            _repo = repo;
        }

        public IActionResult Index()
        {
            var customDates = _repo.CustomDates;
            return View(customDates);
        }
    }
}
