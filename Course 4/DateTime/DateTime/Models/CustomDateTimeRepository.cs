﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DateTimeCustom.Models
{
    public class CustomDateTimeRepository
    {
        readonly List<CustomDateTime> customDates = new List<CustomDateTime>
        {
            new CustomDateTime
            {
                Date = System.DateTime.UtcNow.Date,
                Day = System.DateTime.UtcNow.DayOfWeek,
                Hour = System.DateTime.UtcNow.Hour
            }
        };

        public List<CustomDateTime> CustomDates => customDates;
    }
}
