﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DateTimeCustom.Models
{
    public class CustomDateTime
    {
        public System.DateTime Date { get; set; }
        public System.DayOfWeek Day { get; set;  }  
        public int Hour { get; set; }

    }
}
