#pragma checksum "C:\Cursuri\csharpcourse\Course 4\DateTime\DateTime\Views\CustomDateTime\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "e4f7212509845835597f7e73e47d7c99a96053c3"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_CustomDateTime_Index), @"mvc.1.0.view", @"/Views/CustomDateTime/Index.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/CustomDateTime/Index.cshtml", typeof(AspNetCore.Views_CustomDateTime_Index))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"e4f7212509845835597f7e73e47d7c99a96053c3", @"/Views/CustomDateTime/Index.cshtml")]
    public class Views_CustomDateTime_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<DateTimeCustom.Models.CustomDateTime>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(46, 2, true);
            WriteLiteral("\r\n");
            EndContext();
#line 3 "C:\Cursuri\csharpcourse\Course 4\DateTime\DateTime\Views\CustomDateTime\Index.cshtml"
  
    ViewData["Title"] = "Index";

#line default
#line hidden
            BeginContext(89, 143, true);
            WriteLiteral("\r\n<h2>Custom DateTime</h2>\r\n\r\n<div>\r\n    <h3>Date: Model.Date </h3>\r\n    <h3>Day: Model.Day </h3>\r\n    <h3>Hour: Model.Hour </h3>\r\n</div>\r\n\r\n\r\n");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<DateTimeCustom.Models.CustomDateTime> Html { get; private set; }
    }
}
#pragma warning restore 1591
