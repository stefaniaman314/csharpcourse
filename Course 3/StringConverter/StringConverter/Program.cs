﻿using System;
using System.Collections.Generic;
using System.IO;

namespace StringConverter
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] words = File.ReadAllLines("C:/Cursuri/Curs #/csharpcourse/StringConverter/StringConverter/strings.txt");

            string joinedWords = string.Join(", ", words);

            Console.WriteLine(joinedWords);
        }
    }
}
