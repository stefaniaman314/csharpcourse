﻿using System;
using System.Collections.Generic;

namespace StringFinder
{
    class Program
    {
        static void Main(string[] args)
        {
            List<string> cities = new List<string>()
            { "ROME",
              "LONDON",
              "NAIROBI",
              "CALIFORNIA",
              "ZURICH",
              "NEW DELHI",
              "AMSTERDAM",
              "ABU DHABI",
              "PARIS"
            };

            Console.WriteLine("List of cities:");

            cities.ForEach(Console.WriteLine);

            string result = cities.Find(city => city.StartsWith('A') && city.EndsWith('M'));

            Console.WriteLine("The city starting with 'A' and ending with 'M': " + result);
        }
    }
}
