﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Animal
{
    interface IAnimalRepository
    {
        void Add(Animal animal);
        Animal GetById(int id);
        IEnumerable<Animal> GetAll();
    }

    class AnimalRepository : IAnimalRepository
    {
        readonly List<Animal> _animals = new List<Animal>();

        public void Add(Animal animal)
        {
            _animals.Add(animal);
        }

        public IEnumerable<Animal> GetAll() => _animals;

        public Animal GetById(int id)
        {
            foreach (var animal in _animals)
            {
                if (animal.Id == id)
                {
                    return animal;
                }
            }
            return null;
        }
    }
}
