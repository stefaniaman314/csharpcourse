﻿using System;
using System.Collections.Generic;

namespace Animal
{
    class Program
    {
        static void Main(string[] args)
        {
            Animal a1 = new Animal
            {
                Id = 1,
                Name = "Noodles",
                Description = "hamster"
            };

            Animal a2 = new Animal
            {
                Id = 2,
                Name = "Kali",
                Description = "cat"
            };

            AnimalRepository repo = new AnimalRepository();

            repo.Add(a1);
            repo.Add(a2);

            Animal first = repo.GetById(1);
            Animal second = repo.GetById(2);

            IEnumerable<Animal> animals = repo.GetAll();

            Console.WriteLine("========== Animals =========");

            foreach( Animal animal in animals)
            {
                Console.WriteLine(animal);
            }
        }
    }
}
