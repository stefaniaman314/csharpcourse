﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Text;

namespace Animal.AnimalTest
{
    [TestClass]
    public class AnimalTest
    {
        Animal a1 = new Animal
        {
            Id = 1,
            Name = "Noodles",
            Description = "hamster"
        };

        Animal a2 = new Animal
        {
            Id = 2,
            Name = "Kali",
            Description = "cat"
        };

        [TestMethod]
        public void RetrieveCorrectData_Test()
        {
            AnimalRepository repo = new AnimalRepository();

            repo.Add(a1);
            repo.Add(a2);

            Animal first = repo.GetById(1);
            Animal second = repo.GetById(2);

            Assert.AreEqual(a1.Id, first.Id);
            Assert.AreEqual(a1.Name, first.Name);
            Assert.AreEqual(a1.Description, first.Description);

        }

        [TestMethod]
        public void RetrieveInvalidData_Test()
        {
            AnimalRepository repo = new AnimalRepository();

            repo.Add(a1);
            repo.Add(a2);

            Animal first = repo.GetById(1);
            Animal second = repo.GetById(2);

            Assert.AreEqual(3, first.Id);
            Assert.AreEqual(a1.Name, first.Name);
            Assert.AreEqual(a1.Description, first.Description);

        }
    }
}
