﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Animal.AnimalTest
{
    [TestClass]
    public class AnimalControllerTest
    {
        [TestMethod]
        public void GetData_Test()
        {
            Mock<IAnimalRepository> repoMock = new Mock<IAnimalRepository>();

            Animal a1 = new Animal
            {
                Id = 1,
                Name = "Noodles",
                Description = "hamster"
            };

            repoMock.Setup(x => x.GetById(1)).Returns(a1);

            AnimalController controller = new AnimalController(repoMock.Object);
            Animal result = controller.GetById(1);

            Assert.AreEqual(a1.Id, result.Id);
            Assert.AreEqual(a1.Name, result.Name);
            Assert.AreEqual(a1.Description, result.Description);
        }

        [TestMethod]
        public void GetAllData_Test()
        {
            Mock<IAnimalRepository> mock = new Mock<IAnimalRepository>();

            Animal a1 = new Animal
            {
                Id = 1,
                Name = "Noodles",
                Description = "hamster"
            };

            Animal a2 = new Animal
            {
                Id = 2,
                Name = "Kali",
                Description = "cat"
            };

            List<Animal> animals = new List<Animal>
            {
                a1,
                a2
            };

            mock.Setup(x => x.GetAll()).Returns(animals);

            AnimalController controller = new AnimalController(mock.Object);

            List<Animal> result = controller.GetAll().ToList();

            Assert.AreEqual(2, result.Count);
        }
    }
}
