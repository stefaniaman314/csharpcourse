﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Animal
{
    class AnimalController
    {
        readonly IAnimalRepository _animalRepository;

        public AnimalController(IAnimalRepository animalRepository)
        {
            _animalRepository = animalRepository;
        }

        public AnimalController()
        {
            _animalRepository = new AnimalRepository();
        }

        public void Insert(Animal animal)
        {
            _animalRepository.Add(animal);
        }

        public Animal GetById(int id)
        {
            return _animalRepository.GetById(id);
        }

        public IEnumerable<Animal> GetAll() => _animalRepository.GetAll();
    }
}
