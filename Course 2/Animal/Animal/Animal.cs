﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Animal
{
    class Animal
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public override string ToString() => $"Animal Info: {Id} {Name} {Description}";
    }
}
