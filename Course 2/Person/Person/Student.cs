﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Person
{
    class Student : Person
    {
        public string Number { get; set; }
        public double Score { get; set; }

        public Student(string firstName, string lastName, string address, string number, double score) :
            base(firstName, lastName, address)
        {
            this.Number = number;
            this.Score = score;
        }

        public override string ToString()
        {
            return base.ToString() + $" Number: {Number}, Score: {Score}";
        }
    }
}
