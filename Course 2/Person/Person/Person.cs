﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Person
{
    class Person
    {
        public string FirstName { get; set; }
        public string LastName { get; set;  }
        public string Address { get; set; }

        public Person(string firstName, string lastName, string address)
        {
            this.FirstName = firstName;
            this.LastName = lastName;
            this.Address = address;
        }
        public override string ToString()
        {
            return $"First Name:{FirstName}, Last Name: {LastName}, Address: {Address}";
        }
    }
}
