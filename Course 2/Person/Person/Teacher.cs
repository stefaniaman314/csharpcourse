﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Person
{
    enum ETeacherLevel
    {
        Definitive,
        Level1,
        Level2,
        Phd
    }

    class Teacher : Person
    {
        public ETeacherLevel Level { get; set; }

        public Teacher(string firstName, string lastName, string address, ETeacherLevel level) :
            base(firstName, lastName, address)
        {
            this.Level = level;
        }

        public override string ToString()
        {
            return base.ToString() + $" Level: {Level}";
        }
    }
}
