﻿using System;
using System.Collections.Generic;

namespace Person
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Person> people = new List<Person>();

            Person p1 = new Person("Lucas", "Mill", "5th Avenue");
            Person p2 = new Person("Miriam", "Hall", "6th Avenue");

            Student s1 = new Student("Matt", "Davis", "7th Avenue", "MD123", 9.75);
            Student s2 = new Student("Clara", "Raven", "8th Avenue", "CR000", 8.50);

            people.Add(p1);
            people.Add(p2);
            people.Add(s1);
            people.Add(s2);

            Console.WriteLine("List of people:\n");
            people.ForEach(Console.WriteLine);

            //sort by last name
            people.Sort((pers1, pers2) => pers1.LastName.CompareTo(pers2.LastName));

            Console.WriteLine("\nList of people sorted by last name:\n");
            people.ForEach(Console.WriteLine);

            //sort by first name
            people.Sort((pers1, pers2) => pers1.FirstName.CompareTo(pers2.FirstName));

            Console.WriteLine("\nList of people sorted by last name:\n");
            people.ForEach(Console.WriteLine);
        }
    }
}
